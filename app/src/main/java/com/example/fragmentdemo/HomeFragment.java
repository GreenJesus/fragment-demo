package com.example.fragmentdemo;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class HomeFragment extends Fragment {

    private TextView profileName;
    private ImageView profileImage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        profileName = view.findViewById(R.id.textView2);
        profileImage = view.findViewById(R.id.imageView);
        MainActivity mainActivity = (MainActivity) getActivity();

        profileName.setText(mainActivity.getProfileName());
        profileImage.setImageBitmap(mainActivity.getProfileImage());
        return view;
    }
}