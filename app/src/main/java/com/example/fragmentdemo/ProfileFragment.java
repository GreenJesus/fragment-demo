package com.example.fragmentdemo;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class ProfileFragment extends Fragment {

    private static final int CAMERA_REQUEST_CODE = 100;
    private ImageView profileImageView;
    private EditText profileEditText;
    private Button saveButton;
    private Button cancelButton;
    private Bitmap profileImage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        profileImageView = view.findViewById(R.id.profileImageView);
        profileEditText = view.findViewById(R.id.profileEditText);
        saveButton = view.findViewById(R.id.saveButton);
        cancelButton = view.findViewById(R.id.cancelButton);

        profileImageView.setOnClickListener(v -> {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, CAMERA_REQUEST_CODE);
        });

        MainActivity mainActivity = (MainActivity) getActivity();
        cancelButton.setOnClickListener(v -> {
            mainActivity.navigateToHome();
        });

        saveButton.setOnClickListener(v -> {
            String profileName = profileEditText.getText().toString();
            if(profileImage != null) {
                mainActivity.setProfileImage(profileImage);
            }
            mainActivity.saveAndNavigateToHome(profileName);
        });

        String profileName = mainActivity.getProfileName();
        profileImageView.setImageBitmap(mainActivity.getProfileImage());
        profileEditText.setText(profileName);

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == CAMERA_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            profileImageView.setImageBitmap(photo);
            profileImage = photo;
        }
    }


}