package com.example.fragmentdemo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private String profileName;
    private Bitmap profileImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar mainToolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(mainToolbar);

        loadFragment(new HomeFragment(), "home", false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                loadFragment(new HomeFragment(),"home", true);
                return true;
            case R.id.action_profle:
                loadFragment(new ProfileFragment(),"profile", true);
                return true;
            case R.id.action_setting:
                showMessage("Beállítások");
                Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(intent);
                return true;
        }

        return true;
    }

    // cancel
    public void navigateToHome() {
        loadFragment(new HomeFragment(), "home", true);
    }
    // save
    public void saveAndNavigateToHome(String profileName) {
        this.profileName = profileName;
        navigateToHome();
    }

    public String getProfileName() {
        return  profileName;
    }

    public void setProfileImage(Bitmap profileImage) {
        this.profileImage = profileImage;
    }

    public Bitmap getProfileImage() {
        return profileImage;
    }

    private void loadFragment(Fragment fragment, String tag, boolean addToBacStack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment, tag);
        if (addToBacStack)
            transaction.addToBackStack(tag);
        transaction.commit();
    }

    private void showMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }
}